#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

# install nginx
#apt-get update
#apt-get -y install nginx

# make sure nginx is started
#service nginx start

sudo apt-get update && sudo apt upgrade -y
sudo apt install curl -y


#install nagios

curl https://assets.nagios.com/downloads/nagiosxi/install.sh | sh


